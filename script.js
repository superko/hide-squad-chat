document.body.insertAdjacentHTML(
  'afterbegin',
  `<style>
    div.multi-stream-player-layout__chat {
      display: none !important;
    }
    .multi-stream-player-layout__player-container {
      max-height: 100vh !important;
    }
  </style>`
);