# Hide Squad Chat

A Firefox extension to hide Twitch chat when watching in Squad Mode.

[Install!](https://addons.mozilla.org/en-US/firefox/addon/hide-squad-chat/)

Icon adapted from [Feather Icons](https://feathericons.com/) by Cole Bemis.